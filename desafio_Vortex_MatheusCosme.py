#TREINO DE BASQUETE


#Receber Tempo total do Execício. Onde T será no mínimo 1 e no máximo 24.
while True:
    t = int(input('''Digite o tempo total (T) do execício (em segundos) 
Válidos valores de 1 a 24: '''))
    if t >= 1 and t <= 24:
        break
    else:
        print('Valor Inválido. Tente Novamente')

#Receber a quantidade de jogadores em quadra. Onde Q será no mínimo 2 e no máximo 5.    
while True:    
    q = int(input('''\nDigite a quantidade (Q) de jogadores que participarão 
Válidos valores de 2 a 5: '''))
    if q >= 2 and q <= 5:
        break
    else:
        print('Valor Inválido. Tente Novamente')


#Receber a quantidade de tempo de reposicionamento. Onde X será no mínimo 1 e no máximo Q - 1.
while True:
    x = int(input(f'''\nDigite o tempo de reposicionamento (X)
Válidos valores de 1 a {q-1}: '''))
    if x >= 1 and x <= (q-1):
        break
    else:
        print('Valor Inválido. Tente Novamente')    

disponiveis = q
livres = 0


#Cada segundo teremos um jogador a menos disponível (o jogador que faz o passe)
#E depois dos primeiros X segundos teremos um jogador retornando a disponibilidade a cada segundo (jogador que se reposicionou)

for segundos in range (0, t):
    disponiveis -= 1
    if segundos >= x:
        disponiveis += 1
    livres += disponiveis         

print(f'\nSomatório de jogadores livres para cada passe: {livres}')
    





